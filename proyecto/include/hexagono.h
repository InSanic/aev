#include "sprite.h"

class Hexagono
	{
		private:
			bool* arco;
			sprite* sp;

		public:
			Hexagono(sprite* s);

			bool* getAll()              {return arco;}
			bool getArco(int pos)		{return arco[pos];}
			void addArco(int i)			{arco[i]=1;};
			void removeArco(int i)		{arco[i]=0;};

			void pintar();

			void rotarDerecha();
			void rotarIzquierda();
			void updateSprite();
	};