#include <nds.h>

class sprite
{
	private:
		//static int ID;
		OamState* oam;
		int id;
		int x, y;	//posicion
		
		int priority;
		int palette;
		SpriteSize size;
		SpriteColorFormat colorformat;
		u16* gfx;

		int rotation;

		bool sizedoublerotation;
		bool hide;
		bool vflip;
		bool hflip;
		bool mosaic;
	public:

		sprite(OamState* oam, int id, int x, int y, SpriteSize size, SpriteColorFormat colorformat, 
		const unsigned int* Tiles, int TilesSize, const unsigned short* Pal, int PalSize,
		int priority = 0, int rotation = 0, bool sizedoublerotation = false, bool hide = false, bool hflip = false, bool vflip = false, bool mosaic = false) ;
/*
		sprite(OamState* oam, int x, int y, SpriteSize size, SpriteColorFormat colorformat, 
		const unsigned int* Tiles, int TilesSize, const unsigned short* Pal, int PalSize,
		int priority = 0, int rotation = 0, bool sizedoublerotation = false, bool hide = false, bool hflip = false, bool vflip = false, bool mosaic = false) ;
*/		
		int getId()							{return id;}
		int getX()							{return x;}
		int gety()							{return y;}
		int getPriority()					{return priority;}
		int getPalette()					{return palette;}
		u16* getGFX()						{return gfx;}
		int getRotation()					{return rotation;}
		bool getSizeDoubleRotation()		{return sizedoublerotation;}
		bool getHide()						{return hide;}
		bool getVflip(bool b)				{return vflip;}
		bool getHflip(bool b)				{return hflip;}
		bool getMosaic(bool b)				{return mosaic;}

		void setX(int x2)					{x=x2;}
		void setY(int y2)					{y=y2;}
		void setPosicion(int x2, int y2)	{x=x2;y=y2;}
		void setPriority(bool p)			{priority=p;}
		void setGFX(u16* g)					{gfx=g;}
		void setPalette(int p)				{palette=p;}
		void setRotation(int r)				{rotation=r; }
		void setSizedoublerotation(bool b)	{sizedoublerotation=b;}
		void setHide(bool b)				{hide=b;}
		void setVflip(bool b)				{vflip=b;}
		void setHflip(bool b)				{hflip=b;}
		void setMosaic(bool b)				{mosaic=b;}
		void updateSprite();
};
