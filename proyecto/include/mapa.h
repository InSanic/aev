#include "hexagono.h"
#include <utility>
#include "stdio.h"
#include <stdlib.h> 
#include <time.h> 



#define FILAS 4		//192/32 = 6
#define COLUMNAS 4		//256/32 = 8

#define MAX_HEXAGONOS 16
#define MIN_HEXAGONOS 4

#define RADIO_HEXAGONO 14 * 2
#define ALTURA_HEXAGONO 14 * 2

#define D_HORIZONTAL ALTURA_HEXAGONO * 2 // = ALTURA_HEXAGONO * 2
#define D_VERTICAL RADIO_HEXAGONO *  1.5   // = RADIO_HEXAGONO * 1,5

typedef std::pair<int, int> Posicion;

class Mapa
	{
		private:
			Hexagono* tablero[FILAS][COLUMNAS];
			sprite* sprites[FILAS][COLUMNAS];
			
			//Posicion del centro del circulo del inscrito del hexagono
			Posicion circulos_inscritos[FILAS][COLUMNAS];
			int max;
			int movimientos_mezcla;
			int arcos_totales;
			int arcos_actuales;

			/**
				Rellena el array con un numero binario y cambia la cantidad restante de hexagonos que caben en el mapa
				Si el array ya tiene algo dentro, simplemente lo ignora

				@param dec El valor decimal a pasar a binario que rellena el array
				@param pos Posicion en la que se va a colocar el siguiente valor en el array
				@param array Array que se va a rellenar
				@param x Posicion X del hexagono actual
				@param y Posicion Y del hexagono actual 

			**/
			void addNextHex(int dec, int pos, bool* array, int x, int y); //BUENA SUERTE DEBUGGEANDO

			/**
				Carga en el array propio los hexagonos que tiene alrededor, y ademas, anyade a los hexagonos de alrededor el hexagono actual
				
				@param x Posicion X del hexagono actual
				@param y Posicion Y del hexagono actual 

			**/
			void loadArcos(int x, int y);

			/**
    
				Comprueba si el array de entrada es el mismo que el que tenemos almacenado

				@param array Array que queremos comprobar si es igual almacenado
				@param aux Array que tenemos almacenado

			 **/
			bool check(bool* array, bool* aux);
			/**
 
				Almacena el array de entrada en un array auxiliar

				@param array El array que queremos guardar
				@param aux El array donde se guarda

			**/
			void save(bool* array, bool* aux);

			/**
    
			    Salta al hexagono que ocupa la posicion indicada en los parametros de entrada
			    
			    @param x Posicion X del hexagono al que saltamos
			    @param y Posicion Y del hexagono al que saltamos
			    
			 **/
			void saltarA(int x, int y);

			/**
			  
			    Se encarga de contar cuantos hexagonos adyacentes tiene el hexagono cuyo array se le pasa de entrada
			    
			    @param array Array del hexagono del cual se quiere saber los adyacentes
			    
			**/
			int contarAdyacentes(bool* array);

			/**
			    
			    Elimina del array de entrada el numero indicado de arcos
			    
			    @param eliminar Numero de arcos a eliminar
			    @param array Array del cual se eliminan los arcos
			    @param x Posicion X del hexagono del cual se eliminan los arcos
			    @param y Posicion Y del hexagono del cual se eliminan los arcos
			    
			 **/
			void eliminarAdyacentes(int eliminar, bool* array, int x, int y);

			/**
			    
			    Se encarga de eliminar arcos dejando al menos 1 en cada hexagono
			    
			 **/
			void quitarArcos();

			/**
			    
			    Repara el mapa de posibles conexiones faltantes y/o hexagonos sueltos
			    
			 **/
			void arreglarMapa();

		public:
			Mapa();
			~Mapa();

			void generarMapa(unsigned int semilla = time(NULL));

			/*
				Dada una posicion X, Y de la pantalla te dice a que hexagono de la matriz corresponde

				@param X coordenada X de la pantalla
				@param Y coordenada Y de la pantalla

				@return X,Y del hexagono en cuestion, en el caso de que no exista devuelve -1, -1
			*/
			Posicion transformarCoords(int x, int y);

			/*
				Dada una posicion X, Y de la pantalla te dice a que hexagono de la matriz corresponde

				@param pos coordenadas X,Y de la pantalla

				@return X,Y del hexagono en cuestion, en el caso de que no exista devuelve -1, -1
			*/
			Posicion transformarCoords(Posicion pos)	{return transformarCoords(pos.first, pos.second);}
			/*
				Dadas unas coordenadas X,Y y el lado que nos interesa, 
				nos devuelve las coordenadas X,Y del hexagono que esta en relacionado con nuestro lado de interes
			
				@param x posicion X del hexagono
				@param y posicion X del hexagono
				@param pos lado de interes del hexagono
			*/
			Posicion transformarPos(int x, int y, int pos);

			/*
				Dadas unas coordenadas X,Y y el lado que nos interesa, 
				nos devuelve las coordenadas X,Y del hexagono que esta en relacionado con nuestro lado de interes
			
				@param p posicion X,Y del hexagono
				@param pos lado de interes del hexagono
			*/
			Posicion transformarPos(Posicion p, int pos)			{return transformarPos(p.first, p.second, pos);}
			/*
				Devuelve el hexagono situado en las coordenadas x, y
				
				@param x posicion X del hexagono
				@param y posicion Y del hexagono
			*/
			Hexagono* getHexagono(int x, int y);
			
			/*
				Devuelve el hexagono situado en las coordenadas x, y
				
				@param pos posicion X, Y
			*/
			Hexagono* getHexagono(Posicion pos)						{return getHexagono(pos.first, pos.second);}

			/*
				Devuelve el hexagono situado en la relacion pos del hexagono x,y
				
				@param x posicion X del hexagono
				@param y posicion Y del hexagono
				@param pos el lado del hexagono que nos interesa
			*/
			Hexagono* getHexagono(int x, int y, int pos);
			
			/*
				Devuelve el hexagono situado en la relacion pos del hexagono x,y
				
				@param pos posicion X, Y
				@param pos2 el lado del hexagono que nos interesa
			*/
			Hexagono* getHexagono(Posicion pos, int pos2)						{return getHexagono(pos.first, pos.second, pos2);}

			/*
				Sustituye el hexagono de la posicion x, y por el h
				
				@param h nuevo hexagono
				@param x posicion X del hexagono
				@param y posicion Y del hexagono
			*/
			void setHexagono(Hexagono* h, int x, int y);
			
			/*
				Sustituye el hexagono de la posicion x, y por el h
				
				@param h nuevo hexagono
				@param pos posicion X,Y del hexagono
			*/
			void setHexagono(Hexagono* h, Posicion pos)				{setHexagono(h, pos.first, pos.second);}

			/*
				Anyade un nuevo hexagono en la posicion X, Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono

				@return FALSE si ya existe, o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool addHexagono(int x, int y);
			
			/*
				Anyade un nuevo hexagono en la posicion X, Y

				@param pos posicion X,Y del hexagono
	
				@return FALSE si ya existe o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool addHexagono(Posicion pos)							{return addHexagono(pos.first, pos.second);}

			/*
				Anyade un nuevo hexagono en la relacion pos del hexagono X,Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono
				@param pos el lado del hexagono que nos interesa

				@return FALSE si ya existe, o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool addHexagono(int x, int y, int pos);
			
			/*
				Anyade un nuevo hexagono en la relacion pos del hexagono X,Y

				@param pos posicion X,Y del hexagono
				@param pos2 el lado del hexagono que nos interesa
	
				@return FALSE si ya existe o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool addHexagono(Posicion pos, int pos2)							{return addHexagono(pos.first, pos.second, pos2);}

			/*
				Elimina el hexagono de la posicion X, Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono

				@return FALSE si no hay un hexagono o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool RemoveHexagono(int x, int y);
			
			/*
				Elimina el hexagono de la posicion X, Y

				@param pos posicion X,Y del hexagono

				@return FALSE si no hay un hexagono o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool RemoveHexagono(Posicion pos)						{return RemoveHexagono(pos.first, pos.second);}

			/*
				Anyade el arco pos al hexagono situado en la posicion X,Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono

				@return FALSE si no hay un hexagono o no hay un hexagono al que poner arco
				o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool addArco(int x, int y, int pos);
			
			/*
				Anyade el arco pos al hexagono situado en la posicion X,Y

				@param pos posicion X,Y del hexagono

				@return FALSE si no hay un hexagono o no hay un hexagono al que poner arco
				o la posicion esta fuera del tablero, TRUE en caso contrario
			*/

			bool addArco(Posicion p, int pos)						{return addArco(p.first, p.second,pos);}

			/*
				Elimina el arco pos al hexagono situado en la posicion X,Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono

				@return FALSE si no hay un hexagono o no hay un hexagono al que eliminar arco
				o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool RemoveArco(int x, int y, int pos);

			/*
				Elimina el arco pos al hexagono situado en la posicion X,Y

				@param pos posicion X,Y del hexagono

				@return FALSE si no hay un hexagono o no hay un hexagono al que poner arco
				o la posicion esta fuera del tablero, TRUE en caso contrario
			*/
			bool RemoveArco(Posicion p, int pos)					{return RemoveArco(p.first, p.second,pos);}

			/*
				Devuelve el array de arcos del hexagono de las coordenadas X,Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono

				@return array[6] 
			*/
			bool* getAll(int x, int y)								{return (getHexagono(x,y)) ? getHexagono(x,y)->getAll() : 0;}
			
			/*
				Devuelve el array de arcos del hexagono de las coordenadas X,Y

				@param pos posicion X,Y del hexagono

				@return array[6] 
			*/
			bool* getAll(Posicion pos)								{return getAll(pos.first, pos.second);}


			/*
				Rota a la derecha el hexagono situado en la posicion X,Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono
				@param pintar indica si hay que pintar el hexagono despues de girarlo
			*/
			void rotarDerecha(int x, int y);

			/*
				Rota a la derecha el hexagono situado en la posicion X,Y

				@param pos posicion X,Y del hexagono
				@param pintar indica si hay que pintar el hexagono despues de girarlo
			*/
			void rotarDerecha(Posicion pos)		{rotarDerecha(pos.first, pos.second);}

			/*
				Rota a la izqueirda el hexagono situado en la posicion X,Y

				@param x posicion X del hexagono
				@param y posicion Y del hexagono
				@param pintar indica si hay que pintar el hexagono despues de girarlo
			*/
			void rotarIzquierda(int x, int y);
			
			/*
				Rota a la izqueirda el hexagono situado en la posicion X,Y

				@param pos posicion X,Y del hexagono
				@param pintar indica si hay que pintar el hexagono despues de girarlo
			*/
			void rotarIzquierda(Posicion pos)	{rotarIzquierda(pos.first, pos.second);}

			/*
				Actualiza el sprite

				@param x posicion X del hexagono
				@param y posicion Y del hexagono
			*/
			void updateSprite(int x, int y);

			/*
				Pinta todos los arcos de los hexagonos
			*/
			void pintar();

			/*
				Indica si se ha solucionado el puzle
			*/
			bool checkSolve();	//Version de fuerza bruta
			bool checkSolve2();

			/*
				Rota los hexagonos de forma aleatoria
			*/
			void mezclar();

			/*
				Elimina todos los hexagonos del mapa
			*/
			void limpiar();

			/*
				Devuelve el numero de movmimientos minimos necesarios para resolver el puzle
			*/
			int getMovimientosMinimos()		{	return movimientos_mezcla;	}
	};