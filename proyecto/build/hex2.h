
//{{BLOCK(hex2)

//======================================================================
//
//	hex2, 64x64@4, 
//	+ palette 16 entries, not compressed
//	+ 64 tiles not compressed
//	Total size: 32 + 2048 = 2080
//
//	Time-stamp: 2016-04-14, 12:31:03
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_HEX2_H
#define GRIT_HEX2_H

#define hex2TilesLen 2048
extern const unsigned int hex2Tiles[512];

#define hex2PalLen 32
extern const unsigned short hex2Pal[16];

#endif // GRIT_HEX2_H

//}}BLOCK(hex2)
