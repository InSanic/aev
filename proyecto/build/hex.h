
//{{BLOCK(hex)

//======================================================================
//
//	hex, 32x32@4, 
//	+ palette 16 entries, not compressed
//	+ 16 tiles not compressed
//	Total size: 32 + 512 = 544
//
//	Time-stamp: 2016-04-14, 12:31:03
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_HEX_H
#define GRIT_HEX_H

#define hexTilesLen 512
extern const unsigned int hexTiles[128];

#define hexPalLen 32
extern const unsigned short hexPal[16];

#endif // GRIT_HEX_H

//}}BLOCK(hex)
