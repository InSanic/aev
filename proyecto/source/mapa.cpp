#include "mapa.h"

#include "hex.h"
#include "hex2.h"

Mapa::Mapa()
	{
		max = 0;
		int id = 0;

		/*
			Inicializa el array de tablero
		*/
		for(int i = 0; i < FILAS;i++)
			for(int j=0;j < COLUMNAS;j++)
				tablero[i][j] = 0;  // 0 = NULL

		/*
			Inicializa el array de sprites
		*/
		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
				if(i%2 == 0)
					{
						sprites[i][j] = new sprite(&oamMain, id++, (j*D_HORIZONTAL), (i*D_VERTICAL), SpriteSize_64x64, SpriteColorFormat_16Color, hex2Tiles, hex2TilesLen, hex2Pal, hex2PalLen);
						sprites[i][j]->setHide(true);
					}
				else
					{
						sprites[i][j] = new sprite(&oamMain, id++,(RADIO_HEXAGONO+j*D_HORIZONTAL), (i*D_VERTICAL), SpriteSize_64x64, SpriteColorFormat_16Color, hex2Tiles, hex2TilesLen, hex2Pal, hex2PalLen);
						sprites[i][j]->setHide(true);
					}

		/*
			Hacemos una matriz con el centro de los hexagonos para poder calcular que hexagono se ha pulsado 
			dando las coordenadas de la pantalla
		*/
		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
				if(i%2 == 0)
					circulos_inscritos[i][j] = std::make_pair((RADIO_HEXAGONO+6+j*D_HORIZONTAL), (RADIO_HEXAGONO+6+i*D_VERTICAL));
				else
					circulos_inscritos[i][j] = std::make_pair((RADIO_HEXAGONO+6+RADIO_HEXAGONO+j*D_HORIZONTAL), (RADIO_HEXAGONO+6+i*D_VERTICAL));
	}

Mapa::~Mapa()
	{
		for(int i = 0; i < FILAS;i++)
			for(int j=0;j < COLUMNAS;j++)
				delete tablero[i][j];

		for(int i = 0; i < FILAS;i++)
			for(int j=0;j < COLUMNAS;j++)
				delete sprites[i][j];
	}


Hexagono* Mapa::getHexagono(int x, int y)
	{
		Hexagono *h = 0;

		if(x >= 0 && y >= 0 && x < FILAS && y < COLUMNAS)
			h = tablero[x][y];
			
		return h;
	}

Hexagono* Mapa::getHexagono(int x, int y, int pos)
	{
		Posicion p = transformarPos(x,y,pos);
		x = p.first;
		y = p.second;

		Hexagono *h = 0;

		if(x >= 0 && y >= 0 && x < FILAS && y < COLUMNAS)
			h = tablero[x][y];
			
		return h;
	}

void Mapa::setHexagono(Hexagono* h, int x, int y)
	{
		if(x >= 0 && y >= 0 &&  x < FILAS &&  y < COLUMNAS)
			tablero[x][y] = h;
	}

bool Mapa::addHexagono(int x, int y)
	{
		if(getHexagono(x,y))	//Si ya hay un hexagono en esa posicion
			return 0;

		if(x >= 0 && y >= 0 && x < FILAS && y < COLUMNAS)
			{
				tablero[x][y] = new Hexagono(sprites[x][y]);
				sprites[x][y]->setHide(false);
				sprites[x][y]->updateSprite();
				return 1;
			}

		return 0;
	}

bool Mapa::addHexagono(int x, int y, int pos)
	{
		Posicion p = transformarPos(x,y,pos);
		x = p.first;
		y = p.second;

		if(getHexagono(x,y))	//Si ya hay un hexagono en esa posicion
			return 0;

		if(x >= 0 && y >= 0 && x < FILAS && y < COLUMNAS)
			{
				tablero[x][y] = new Hexagono(sprites[x][y]);
				sprites[x][y]->setHide(false);
				sprites[x][y]->updateSprite();
				return 1;
			}

		return 0;
	}

bool Mapa::RemoveHexagono(int x, int y)
	{
		if(!getHexagono(x,y))	//Si no hay un hexagono en esa posicion
			return 0;

		if(x >= 0 && y >= 0 && x < FILAS && y < COLUMNAS)
			{
				sprites[x][y]->setHide(true);
				sprites[x][y]->updateSprite();

				for(int pos=0;pos<6;pos++)
					RemoveArco(x,y,pos);

				Hexagono* h = getHexagono(x, y);

				delete tablero[x][y];

				tablero[x][y] = 0;

				return 1;
			}

		return 0;
	}

Posicion Mapa::transformarCoords(int x, int y)
	{
		for(int i = 0; i < FILAS;i++)
				for(int j=0;j < COLUMNAS;j++)
					{
						int dx = x - circulos_inscritos[i][j].first;
						int dy = y - circulos_inscritos[i][j].second;
						dx*=dx;
						dy*=dy;

						if( (ALTURA_HEXAGONO*ALTURA_HEXAGONO) > (dx+dy))
								return std::make_pair(i,j);
					}

		return std::make_pair(-1,-1);
	}

Posicion Mapa::transformarPos(int x, int y, int pos)
	{
		Posicion p;

		switch(pos)
			{
				case 0:
					if(x%2)	//IMPAR
						p = std::make_pair(x-1, y);
					else	//PAR
						p = std::make_pair(x-1, y-1);
				break;

				case 1:
					if(x%2)	//IMPAR
						p = std::make_pair(x-1, y+1);
					else	//PAR
						p = std::make_pair(x-1, y);
				break;

				case 2:
					p = std::make_pair(x, y+1);
				break;

				case 3:
					if(x%2)	//IMPAR
						p = std::make_pair(x+1, y+1);
					else	//PAR
						p = std::make_pair(x+1, y);
				break;

				case 4:
					if(x%2)	//IMPAR
						p = std::make_pair(x+1, y);
					else	//PAR
						p = std::make_pair(x+1,y-1);
				break;

				case 5:
					p = std::make_pair(x, y-1);
				break;

				default:
					p = std::make_pair(-1, -1);
				break;
			}

		return p;
	}

bool Mapa::addArco(int x, int y, int pos)
	{
		if(pos < 0 || pos > 5)
			return 0;

		Posicion p = transformarPos(x,y,pos);
		int pos2 = (pos+3)%6;
		if(Hexagono* h = getHexagono(x,y))	//Si el arco pertenece a un hexagono del tablero
			if(Hexagono* h2 = getHexagono(p.first, p.second))	//Si el arco conecta con un hexagono DENTRO del tablero
				//if(!h->getArco(pos) && !h2->getArco(pos2)) //Si ya existe ese arco
					{
						h->addArco(pos);
						h2->addArco(pos2);

						return 1;
					}

		return 0;   //Si no existen ambos hexagonos y/o ya estan relacionados
	}

bool Mapa::RemoveArco(int x, int y, int pos)
	{
		if(pos < 0 || pos > 5)
			return false;

		Posicion p = transformarPos(x,y,pos);
		int pos2 = (pos+3)%6;

		if(Hexagono* h = getHexagono(x,y))	//Si el arco pertenece a un hexagono del tablero
			if(Hexagono* h2 = getHexagono(p.first, p.second))	//Si el arco conecta con un hexagono DENTRO del tablero
				if(h->getArco(pos) || h2->getArco(pos2)) //Si existe ese arco
					{
						h->removeArco(pos);
						h2->removeArco(pos2);
						return 1;
					}

		return 0;   //Si no existen ambos hexagonos y/o ya estan relacionados
	}

void Mapa::limpiar()
	{
		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
			{
				sprites[i][j]->setHide(true);
				sprites[i][j]->updateSprite();

				delete tablero[i][j];
				tablero[i][j] = 0;
			}
	}

void Mapa::rotarDerecha(int x, int y)
	{
		if(Hexagono* h = getHexagono(x,y))
			{
				int arcos_antes = 0;
				for(int k = 0; k<6;k++)
					if(h->getArco(k))
						{
							Posicion p = transformarPos(x,y,k);
							Hexagono* h2 = getHexagono(p);
							int pos2 = (k+3)%6;

							if(h2 && h2->getArco(pos2))
								arcos_antes++;
						}

				h->rotarDerecha();
				int arcos_despues = 0;

				for(int k = 0; k<6;k++)
					if(h->getArco(k))
						{
							Posicion p = transformarPos(x,y,k);
							Hexagono* h2 = getHexagono(p);
							int pos2 = (k+3)%6;

							if(h2 && h2->getArco(pos2))
								arcos_despues++;
						}
				arcos_actuales += arcos_despues - arcos_antes;
			}
	}

void Mapa::rotarIzquierda(int x, int y)
	{
		if(Hexagono* h = getHexagono(x,y))
			{
				int arcos_antes = 0;
				for(int k = 0; k<6;k++)
					if(h->getArco(k))
						{
							Posicion p = transformarPos(x,y,k);
							Hexagono* h2 = getHexagono(p);
							int pos2 = (k+3)%6;

							if(h2 && h2->getArco(pos2))
								arcos_antes++;
						}

				h->rotarIzquierda();
				int arcos_despues = 0;

				for(int k = 0; k<6;k++)
					if(h->getArco(k))
						{
							Posicion p = transformarPos(x,y,k);
							Hexagono* h2 = getHexagono(p);
							int pos2 = (k+3)%6;

							if(h2 && h2->getArco(pos2))
								arcos_despues++;
						}

				arcos_actuales += arcos_despues - arcos_antes;
			}
	}

void Mapa::updateSprite(int x, int y)
	{
		if(Hexagono* h = getHexagono(x,y))
			h->updateSprite();
	}

void Mapa::pintar()
	{
		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
				if(Hexagono* h = getHexagono(i,j))
					h->pintar();

	}

bool Mapa::checkSolve()
	{
		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
				if(Hexagono* h = getHexagono(i,j))
					for(int k=0;k<6;k++)
						if(h->getArco(k))
							{
								Posicion p = transformarPos(i,j,k);
								Hexagono* h2 = getHexagono(p);
								int pos2 = (k+3)%6;

								if(!h2 || !h2->getArco(pos2))
									return 0;
							}

		return 1;
	}

bool Mapa::checkSolve2()	{	return arcos_actuales == arcos_totales;}

void Mapa::mezclar()
	{
		/*
			Contamos los arcos totales
		*/
		arcos_totales = 0;

		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
				if(Hexagono* h = getHexagono(i,j))
					for(int k=0;k<6;k++)
						arcos_totales+=h->getArco(k);

		arcos_totales/=2;
		arcos_actuales = arcos_totales;

		/*
			mezcla los hexagonos
		*/	
		movimientos_mezcla = 0;

		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
				{
					if(!getHexagono(i,j))
						continue;
					int movimientos = rand()%5+1;
					movimientos_mezcla += movimientos;
					
					for(int k=0; k < movimientos; k++)
						rotarIzquierda(i,j);	
				}	
	}

/*
	SISTEMA DE GENERACION
*/


void Mapa::addNextHex(int dec, int pos, bool* array, int x, int y) //BUENA SUERTE DEBUGGEANDO
{
    
    if(dec/2&&max>0) //SI podemos dividir y NO hemos alcanzado el maximo de hexagonos
    {
            if(!array[pos]) //Si esta vacia la posicion
            {
				 if(dec%2) //Si toca colocar hexagono
                {   
                    if(addHexagono(x, y, pos)) //Si se coloca con exito
                    {
                        //Añadimos el arco y decrementamos los hexagonos maximos
                        addArco(x, y, pos); 
                        --max;
                    }
                    else //No se ha podido colocar el hexagono
                        if(!array[(pos+1)%6]) //Miramos si la siguiente posicion esta vacia 
                        {   
                            //Hacemos el paso de la siguiente llamada recursiva aqui
                            //Aumentamos la posicion a la posicion vacia y avanzamos al siguiente bit
                            pos = (pos+1)%6;;
                            dec /= 2;
                            if(addHexagono(x, y, pos)) //Si se coloca
                            {
                                //Decrementamos el maximo de hexagonos y añadimos el arco
                                addArco(x, y, pos);
                                --max;
                            }
                        }
                }
            }
            addNextHex(dec/2, pos+1, array, x, y); //Siguiente llamada recursiva
        
    }
    //NO TOCAR QUE SE ROMPE
    if(!array[pos]&&max>0) //Para colocar el ultimo 1 cuando es impar y quedan hexagonos
    {
        if(dec%2) //Si toca colocar Hexagono
        {
            if(addHexagono(x, y, pos)) //Intentamos colocar el hexagono
            {
                //Al colocarlos, tenemos que restarlo de los maximos que caben y añadirlo a nuestro array
                addArco(x, y, pos);
                --max;
            }
            else //No hemos podido colocar el hexagono
            {
                bool terminar = false;
                while(!terminar) //Bucle que determina que haya al menos una conexion a algun hexagono adyacente
                {
                    pos = (pos+1)%6;
                    if(!array[pos] && addHexagono(x, y,pos)) //Si la siguiente posicion esta vacia y podemos colocarlo
                    {
                        //Colocamos el arco, disminuimos el numero de hexagonos maximo y terminamos el bucle 
                        --max;
                        addArco(x,y,pos);
                        terminar = true;
                    }
                    if(array[pos])
                        terminar = true;
                }
            }
        }
    }
        
}


void Mapa::loadArcos(int x, int y)
{
    for(int i=0; i<6; i++)
        addArco(x, y, i);
}


bool Mapa::check(bool* array, bool* aux)
{
    int cont = 0;
    for(int i =0; i<6; i++){
        if(aux[i] == array[i])
            cont++;
    }
    if(cont == 6)
        return false;
    return true;
}


void Mapa::save(bool* array, bool* aux)
{
    for(int i = 0; i<6; i++)
        aux[i] = array[i];
}


void Mapa::saltarA(int x, int y)
{
    int num = rand()%63+1;
	
	//printf("Num = %d\n", num);
    
	
    //Cargar el array con los que tiene alrededor y añade sus arcos
    loadArcos(x, y);
	
    //printf("%d %d\n", x, y);
    //printf("MAX ant = %d\n", max);
	
	
    //Recuperamos el array
    bool* array = getAll(x,y); 
    
	//Comprobar si es NULL
	if(array==NULL)  
		return;
	/*AQUI TENEMOS EL ARRAY CARGADO CON LOS ADYACENTES Y AÑADIDOS LOS ARCOS ENTRE ELLOS*/ 
	/*
    for(int i =0; i<6; i++){
        printf("%d ", array[i]);
    }
    printf("\n");
	*/
    //Guardamos el array en otro sitio para usarlo mas tarde
    bool aux[6];
    save(array, aux);
	
    //Añadimos nuevos hexagonos
    addNextHex(num, 0, array, x, y);
	
	/*AQUI TENEMOS EL ARRAY CARGADO CON LOS NUEVOS Y CREADOS LOS NUEVOS ADYACENTES Y SUS ARCOS*/
	//printf("MAX desp = %d\n", max);
    /*
    for(int i =0; i<6; i++)
        printf("%d ", array[i]);
    printf("\n");
    */
    
    //Comprobamos que se han colocado nuevos hexagonos
    if(!check(array, aux)){
        //printf("Return\n");
        return;
    }
    
    
    /*AQUI SABEMOS QUE AL MENOS SE HA AÑADIDO 1 HEXAGONO A LOS QUE YA TENIAMOS ADYACENTES*/
    
	//Avanzamos a los nuevos hexagonos creados
	//POSIBLE RANDOM
	int init_pos = rand()%6; //Posicion inicial en la que se colocara para empezar a generar los hijos
	int direccion =   rand()%2; //Direccion en la que se desplaza por el array generando los hijos
	for(int i = 0; i < 6; i++)
	{
	    if(array[init_pos]&&!aux[init_pos]) //Si hay hexagono en este lado que sea nuevo
	    {
	        //Cojemos sus coordenadas y saltamos al recojido
	        Posicion p = transformarPos(x,y,init_pos);
	        if(getHexagono(p.first, p.second)!=NULL) //Problema de que salta aun sabiendo que ya estaba colocado de antes
	            saltarA(p.first, p.second);
	    }
	    if(direccion){
	        init_pos = (init_pos+1)%6; //Aumenta en 1 la posicion donde mirar
	    }
	    else{
	        init_pos = ((init_pos-1)+6)%6; //Disminuye en 1 la posicion donde mirar, evitando numeros negativos
	    }
	 }
	
	//printf("FIN\n");
	
}


int Mapa::contarAdyacentes(bool* array)
{
    int cont = 0;
    for(int i = 0; i<6;i++)
        cont += array[i];
    return cont;
}


void Mapa::eliminarAdyacentes(int eliminar, bool* array, int x, int y)
{
    int direccion = rand()%2;
    int pos = rand()%6;
    for(int i = 0;i<6&&eliminar>0; i++)
    {
        if(array[pos])
        {
            //RemoveArco(x, y, pos);
            array[pos] = 0;
            eliminar--;
        }
        if(direccion)
        {
            pos = (pos+1)%6;
        }
        else
        {
            pos = ((pos-1)+6)%6;
        }
    }
}


/**
    
    Se encarga de eliminar arcos dejando al menos 1 en cada hexagono
    
 **/
void Mapa::quitarArcos()
{
    int eliminar;
    for(int i = 0; i<FILAS; i++)
    {
        for(int j = 0; j<COLUMNAS; j++)
        {
            if(getHexagono(i,j)!=NULL) //Si hay hexagono 
            {
                bool* array = getAll(i,j);
                //for(int k=0; k<6;k++)
                    //printf("%d ",array[k]);
                //printf("\n");
                eliminar = rand()%(contarAdyacentes(array)); //Eliminamos de 0 a adyacentes-1
                //printf("ELIMINAR = %d\n", eliminar);
                eliminarAdyacentes(eliminar, array, i, j);
                //for(int k=0; k<6;k++)
                    //printf("%d ",array[k]);
                //printf("\n");
            }
        }
    }
}

void Mapa::arreglarMapa()
{
    for(int i = 0; i<FILAS; i++)
    {
        for(int j = 0; j<COLUMNAS; j++)
        {
            if(getHexagono(i, j))
            {
                bool* array = getAll(i,j);
                for(int k=0;k<6;k++)
                    if(array[k])
                        addArco(i, j, k);
                
            }
        }
    }
}


void Mapa::generarMapa(unsigned int semilla)
{
	//srand(time(NULL));
	srand(semilla);
	int i = rand()%(FILAS);
	int j = rand()%(COLUMNAS);
	max  = rand()%(MAX_HEXAGONOS-MIN_HEXAGONOS+1)+MIN_HEXAGONOS;
	//Colocar en mapa
    addHexagono(i, j);
    --max;
	saltarA(i, j);
	//HASTA AQUI FUNCIONA BIEN 100% SEGURO
	//printf("ELIMINAR\n");
	/*
	for(int k=0;k<FILAS;k++)
		for(int v=0;v<COLUMNAS;v++)
			if(getHexagono(k, v))
			{
                bool* array = getAll(k,v);
                for(int z=0;z<6;z++)
                    printf("%d ",array[z]);
                printf("\n");
			}
    */
	for(int i=0;i<6;i++)
	{
		quitarArcos();
		arreglarMapa();
	}
}
