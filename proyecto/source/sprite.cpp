#include "sprite.h"
#include "stdio.h"


//int sprite::ID = 0;

sprite::sprite(OamState* oam, int id, int x, int y, SpriteSize size, SpriteColorFormat colorformat, 
	const unsigned int* Tiles, int TilesSize, const unsigned short* Pal, int PalSize,
	int priority, int rotation, bool sizedoublerotation, bool hide, bool hflip, bool vflip, bool mosaic)
	{
		this->id = id;
		this->palette = id;
		this->oam = oam;
		this->x = x;
		this->y = y;
		this->priority = priority;
		this->size = size;
		this->colorformat = colorformat;
		this->rotation = rotation;
		this->sizedoublerotation = sizedoublerotation;
		this->hide = hide;
		this->hflip = hflip;
		this->vflip = vflip;
		this->mosaic = mosaic;

		//Inicializamos el 
		this->gfx = oamAllocateGfx(oam, size, colorformat);

		//Cargamos en memoria los Tiles y las paletas del sprite
		dmaCopyHalfWords( 3,Tiles, this->gfx, TilesSize);
		dmaCopyHalfWords( 3,Pal, SPRITE_PALETTE+(this->palette*16), PalSize);
	}
/*
sprite::sprite(OamState* oam, int x, int y, SpriteSize size, SpriteColorFormat colorformat, 
	const unsigned int* Tiles, int TilesSize, const unsigned short* Pal, int PalSize,
	int priority, int rotation, bool sizedoublerotation, bool hide, bool hflip, bool vflip, bool mosaic)
	{
		printf("%d\n", ID-16);
		this->id = ID-16;
		this->palette = ID-16;
		ID++;
		this->oam = oam;
		this->x = x;
		this->y = y;
		this->priority = priority;
		this->size = size;
		this->colorformat = colorformat;
		this->rotation = rotation;
		this->sizedoublerotation = sizedoublerotation;
		this->hide = hide;
		this->hflip = hflip;
		this->vflip = vflip;
		this->mosaic = mosaic;

		//Inicializamos el 
		this->gfx = oamAllocateGfx(oam, size, colorformat);

		//Cargamos en memoria los Tiles y las paletas del sprite
		dmaCopyHalfWords( 3,Tiles, this->gfx, TilesSize);
		dmaCopyHalfWords( 3,Pal, SPRITE_PALETTE+(this->palette*16), PalSize);
	}
*/
void sprite::updateSprite()
	{
		if(rotation)
				oamRotateScale(oam, id, degreesToAngle(rotation), intToFixed(1, 8), intToFixed(1, 8));

		oamSet(oam, //main graphics engine context
		id,           //oam index (0 to 127)  
		x, y,   //x and y pixle location of the sprite
		priority,                    //priority, lower renders last (on top)
		palette,               //this is the palette index if multiple palettes or the alpha value if bmp sprite 
		size,     
		SpriteColorFormat_16Color, 
		gfx,                  //pointer to the loaded graphics
		(rotation) ? id : -1,                  //sprite rotation/scale matrix index 
		sizedoublerotation,               //double the size when rotating?
		hide,         //hide the sprite?
		vflip, hflip, //vflip, hflip
		mosaic //apply mosaic
 		);   
	}