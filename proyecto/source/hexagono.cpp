#include "hexagono.h"
#include "stdio.h"

Hexagono::Hexagono(sprite* s)
	{
		sp = s;
		arco = new bool[6];

		for(int i = 0;i<6;i++)
			arco[i]=0;
	}

void Hexagono::pintar()
	{
		for(int i = 0; i<6;i++)
			{
				if(arco[(i)])
					SPRITE_PALETTE[(sp->getPalette())*16+(i+2)] = RGB15(31,0,0);
				else
					SPRITE_PALETTE[(sp->getPalette())*16+(i+2)] = RGB15(31,31,31);
			}
	}

void Hexagono::rotarDerecha()
	{
		bool aux = arco[5];
		arco[5] = arco[4];
		arco[4] = arco[3];
		arco[3] = arco[2];
		arco[2] = arco[1];
		arco[1] = arco[0];
		arco[0] = aux;
		//sp->setRotation(sp->getRotation()+60);
		//pintar();
	}

void Hexagono::rotarIzquierda()
	{
		bool aux = arco[0];
		arco[0] = arco[1];
		arco[1] = arco[2];
		arco[2] = arco[3];
		arco[3] = arco[4];
		arco[4] = arco[5];
		arco[5] = aux;
		//sp->setRotation(sp->getRotation()-60);
		//pintar();
	}

void Hexagono::updateSprite()
	{
		if(sp)
			sp->updateSprite();
	}