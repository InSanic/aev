	#include <nds.h>
	#include <stdio.h>
	#include "mapa.h"
	#include <fat.h>
	#include <filesystem.h>
	#include <maxmod9.h>
	#include "soundbank.h"
	#include "soundbank_bin.h"


	PrintConsole topScreen;
	touchPosition touch;
	Mapa *map;

	void loadSprites();
	void updateSprites();
	void Setup();
	void nuevoPuzle();
	void printInfo(int movimientos, int sonido);

	//---------------------------------------------------------------------
	// main
	//---------------------------------------------------------------------
	int main(void) 
	{
		Setup();
		
		//Init Maxmod with the default settings
		mmInitDefaultMem( (mm_addr)soundbank_bin );

		mmLoad(MOD_BREAKS);
		mmLoadEffect(SFX_CANNON);
		mmLoadEffect(SFX_GOAL);
		mmStart(MOD_BREAKS,MM_PLAY_LOOP);
		
		loadSprites();
		nuevoPuzle();

		int movimientos = 0;
		bool sonido = true;

		printInfo(movimientos,sonido);
		while(1) 
		{
			scanKeys();

			if(keysDown() & KEY_B)
				{
						consoleClear();
					 	printf("\n\n\n");
					 	printf("\t\t\t\x1b[31;1mU\x1b[37;1mmbr\x1b[31;1me\x1b[37;1mll\x1b[31;1ma \x1b[37mPuzzle\n");
					 	printf("\n\n\n");
					 	printf("\tDesarrollado por:\n\n");
					 	printf("\t\tCarlos Cano\n");
					 	printf("\t\tVictor Zaragoza\n");
					 	printf("\t\tSantiago Navarro\n");
				}
			
		if((keysDown() & KEY_A) && map->checkSolve2())
			{
				consoleClear();
				nuevoPuzle();
				movimientos=0;

				printInfo(movimientos,sonido);
			}

			//Si pulsas L pausas la musica de fondo	
			if(keysDown() & KEY_L)
				{
					if(sonido)
							mmPause();
					else
							mmResume();
					sonido = !sonido;

					printInfo(movimientos,sonido);
				}

					
			//Si pulsas la pantalla y no esta resuelto
			if((keysDown() & KEY_TOUCH) && !map->checkSolve2())
				{
					touchRead(&touch);
					Posicion p = map->transformarCoords(touch.px, touch.py); 
					 if(map->getHexagono(p))
						{
							mm_sfxhand sound_handle = mmEffect(SFX_CANNON);
							mmEffectRelease(sound_handle);
							mmEffectVolume(sound_handle,255);
							map->rotarDerecha(p);
							map->pintar();
							movimientos++;

							printInfo(movimientos,sonido);
						}

					if(map->checkSolve2())
						{
							consoleClear();
							mmPause();
							mm_sfxhand sound_handle2 = mmEffect(SFX_GOAL);
							mmEffectRelease(sound_handle2);
							mmEffectVolume(sound_handle2,255);
							printf("\n\n\n\n\n");
							printf("\t\t\tPuzle Resuelto\n");
							printf("\n\n\n");
							printf("\tMovimientos Minimos: %d\n", map->getMovimientosMinimos());
							printf("\tMovimientos Realizados %d\n", movimientos);
							printf("\n\n\n\n\t Pulsa A para reiniciar\n");
						}
				}

			updateSprites();
			swiWaitForVBlank();
		}

		return 0;
	}

void Setup()
	{
		videoSetMode(MODE_0_2D);
		videoSetModeSub(MODE_0_2D);

		vramSetBankA(VRAM_A_MAIN_SPRITE);
		vramSetBankD(VRAM_D_SUB_SPRITE);

		consoleInit(&topScreen, 3,BgType_Text4bpp, BgSize_T_256x256, 31, 0, false, true);
		consoleSelect(&topScreen);
		lcdMainOnBottom();

		cpuStartTiming(0);
		cpuStartTiming(1);
		cpuStartTiming(2);
	}

void loadSprites()
	{
		//Inicializamos OAM
		oamInit(&oamMain, SpriteMapping_1D_128, false);
		map = new Mapa();
	}

void nuevoPuzle()
	{
		map->limpiar();
		map->generarMapa(timerTick(0)+timerTick(1)+timerTick(2));
		map->mezclar();
		map->pintar();
		mmStart(MOD_BREAKS,MM_PLAY_LOOP);
	}

void updateSprites()
	{
		for(int i=0;i<FILAS;i++)
			for(int j=0;j<COLUMNAS;j++)
					map->updateSprite(i,j);

		oamUpdate(&oamMain);
	}

void printInfo(int movimientos, int sonido)
{
	consoleClear();
	printf("\n\n\n\n");
	printf("Movimientos Minimos: %d\n", map->getMovimientosMinimos());
	printf("Movimientos Realizados %d\n", movimientos);
	printf("\n\n\n\n\n\n");
	printf("Pulsa B para ver los creditos\n");
	if(sonido)
		printf("Pulsa L para PAUSAR el sonido\n");
	else
		printf("Pulsa L para REANUDAR el sonido\n");
}
