#include <nds.h>
#include <fat.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

PrintConsole topScreen;

int getRed(int color)		{	return color & (31 << 16);	}
int getGreen(int color)		{	return color & (31<<8);		}
int getBlue(int color)		{	return color & 31;			}

void SaveImage(void)
	{
		FILE* fp = fopen("Imagen.ppm","w");
		if(fp)
			{
				fprintf(fp,"P3\n");
				fprintf(fp,"%d %d\n",SCREEN_WIDTH, SCREEN_HEIGHT);
				fprintf(fp,"31");

				int i, j;
				for(i=0;i< SCREEN_HEIGHT; i++)
					{
						for(j = 0; j < SCREEN_WIDTH; j++)
						fprintf(fp, "%d %d %d ", getRed(VRAM_A[i]), getGreen(VRAM_A[i]), getBlue(VRAM_A[i]));
						fprintf(fp, "\n");
					}

				fclose(fp);
				iprintf("Archivo guardado como Imagen.ppm\n");
			}
		else
			iprintf("Fallo abriendo archivo");
	}

void ClearScreen(void)
	{
		int i;
		for(i = 0; i < SCREEN_WIDTH   * SCREEN_HEIGHT; i++)
			VRAM_A[i] = RGB15(0,0,0);
	}

int main(void)
	{
		touchPosition touch;

		videoSetModeSub(MODE_0_2D);
		vramSetBankC(VRAM_C_SUB_BG);
		consoleInit(&topScreen, 3,BgType_Text4bpp, BgSize_T_256x256, 31, 0, false, true);

		videoSetMode(MODE_FB0);
		vramSetBankA(VRAM_A_LCD);

		consoleSelect(&topScreen);

		if(!fatInitDefault()) 
			printf("Init FAT: Error!\n");
		else
			printf("Init FAT: OK\n");

		//notice we make sure the main graphics engine renders
		//to the lower lcd screen as it would be hard to draw if the
		//pixels did not show up directly beneath the pen
		lcdMainOnBottom();
		int color = RGB15(31,31,31);;	//blanco
		while(1)
			{
				scanKeys();
				if(keysHeld() & KEY_B)
					ClearScreen();

				if(keysHeld() & KEY_A)
					color = rand();

				if(keysHeld() & KEY_R)
					SaveImage();

				if(keysHeld() & KEY_TOUCH)
					{
						// write the touchscreen coordinates in the touch variable
						touchRead(&touch);
						VRAM_A[touch.px + touch.py * SCREEN_WIDTH] = color;
					}
			}

		return 0;
	}