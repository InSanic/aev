#include <nds.h>
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------
int i;
//point our video buffer to the start of bitmap background video
u16* video_buffer_main = (u16*)BG_BMP_RAM(0);
u16* video_buffer_sub = (u16*)BG_BMP_RAM_SUB(0);
//set video mode to mode 5 with background 3 enabled
videoSetMode(MODE_5_2D | DISPLAY_BG3_ACTIVE);
videoSetModeSub(MODE_5_2D | DISPLAY_BG3_ACTIVE);
//map vram a to start of main background graphics memory
vramSetBankA(VRAM_A_MAIN_BG_0x06000000);
vramSetBankC(VRAM_C_SUB_BG_0x06200000);
//initialize the background
BACKGROUND.control[3] = BG_BMP16_256x256 | BG_BMP_BASE(0);
BACKGROUND.bg3_rotation.hdy = 0;
BACKGROUND.bg3_rotation.hdx = 1 << 8;
BACKGROUND.bg3_rotation.vdx = 0;
BACKGROUND.bg3_rotation.vdy = 1 << 8;
//initialize the sub background
BACKGROUND_SUB.control[3] = BG_BMP16_256x256 | BG_BMP_BASE(0);
BACKGROUND_SUB.bg3_rotation.hdy = 0;
BACKGROUND_SUB.bg3_rotation.hdx = 1 << 8;
BACKGROUND_SUB.bg3_rotation.vdx = 0;
BACKGROUND_SUB.bg3_rotation.vdy = 1 << 8;
//paint the main screen red
for(i = 0; i < 256 * 256; i++)
video_buffer_main[i] = RGB15(31,0,0) | BIT(15);
//paint the sub screen blue
for(i = 0; i < 256 * 256; i++)
video_buffer_sub[i] = RGB15(0,0,31) | BIT(15);
while(1) {
swiWaitForVBlank();
}
return 0;
}