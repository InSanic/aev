#include <nds.h>
#include <stdio.h>
int main(void)
    {
    consoleDemoInit();
    while(1)
    {
    scanKeys();
    int held = keysHeld();
    if( held & KEY_A)
    printf("Key A is pressed\n");
    else
    printf("Key A is released\n");
    if( held & KEY_X)
    printf("Key X is pressed\n");
    else
    printf("Key X is released\n");
    if( held & KEY_TOUCH)
    printf("Touch pad is touched\n");
    else
    printf("Touch pad is not touched\n");
    swiWaitForVBlank();
    consoleClear();
    }
    return 0;
    }