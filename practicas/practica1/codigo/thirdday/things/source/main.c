#include<nds.h>
#include<stdlib.h>
int main(void)
{
    touchPosition touch;
    videoSetMode(MODE_FB0);
    vramSetBankA(VRAM_A_LCD);
    //notice we make sure the main graphics engine renders
    //to the lower lcd screen as it would be hard to draw if the
    //pixels did not show up directly beneath the pen
    lcdMainOnBottom();
    while(1)
    {
    scanKeys();
    if(keysHeld() & KEY_TOUCH)
    {
    // write the touchscreen coordinates in the touch variable
    touchRead(&touch);
    VRAM_A[touch.px + touch.py * 256] = rand();
    }
    }
    return 0;
}