#include <nds.h>
#include <stdio.h>
#include <stdlib.h>




struct Category
{
	const char* name;
	int count;
};

struct Category categories[] = 
{
	{"Alarma", 0},
	{"Cumple", 0},
	{"Nombre", 0},
	{"Tema", 0},
	{"Idioma", 0},
	{"Cambio de pantalla", 0}

};


//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------
	int keys;
	

	
	while(1) {
		int selectedCategory = 0;
		
		bool selected = false;
		
		int catCount = sizeof(categories) / sizeof(Category);
		
		videoSetModeSub(MODE_0_2D);
		consoleDemoInit();
		
		while(!selected) {

			scanKeys();
			
			keys = keysDown();
			
			if(keys & KEY_UP) selectedCategory--;
			if(keys & KEY_DOWN) selectedCategory++;
			if(keys & KEY_A) selected = true;
			
			if(selectedCategory < 0) selectedCategory = catCount - 1;
			if(selectedCategory >= catCount) selectedCategory = 0;
			
			swiWaitForVBlank();
			consoleClear();
			for(int ci = 0; ci < catCount; ci++) {
				iprintf("%c%d: %s\n", ci == selectedCategory ? '*' : ' ', ci + 1, categories[ci].name); 		
			}	
		}
		
		bool swap = 1;
		
		while(1) {

			scanKeys();
			
			keys = keysDown();
			
			if(keys & KEY_B){
					swap=1;
					break;
				} 
			
			
			swiWaitForVBlank();
			consoleClear();
			
			switch(selectedCategory){
				
				case 0: iprintf("%s puesta a las %d:%d",categories[selectedCategory].name,PersonalData->alarmHour,PersonalData->alarmMinute);
				break;
				
				case 1: iprintf("%s: %d-%d",categories[selectedCategory].name,PersonalData->birthDay,PersonalData->birthMonth);
				break;
				
				case 2: 
					iprintf("%s: ",categories[selectedCategory].name);

					for(int i=0;i<10;i++)
						iprintf("%s", PersonalData->name+i);
				break;
				
				case 3: iprintf("%s: %d",categories[selectedCategory].name,PersonalData->theme);
				break;
				
				case 4: iprintf("%s: %d",categories[selectedCategory].name,PersonalData->language);
				break;
				
				case 5: iprintf("%s: ",categories[selectedCategory].name);
						if(swap){
							lcdSwap();
							swap=0;
							}
				break;
				}
				
		}
		
	}
	
}


