#include "nds.h"

#ifndef BALL_H
#define BALL_H

#define c_radius (8<<8)

typedef struct  t_ball
{
	int x, y, xvel, yvel;
	//u8 daba error
	u8 sprite_index, sprite_affine_index;
	int height;
} ball;

void ballUpdate( ball* b );
void ballRender( ball* b, int camera_x, int camera_y );

#endif
